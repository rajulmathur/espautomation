package FileEncryptionTestCode;

import com.google.gson.JsonObject;
import com.microsoft.azure.sdk.iot.device.*;
import com.microsoft.azure.sdk.iot.device.DeviceTwin.*;
import com.microsoft.azure.sdk.iot.device.transport.IotHubConnectionStatus;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class UpdateDeviceTwin {

	private static AtomicBoolean Succeed = new AtomicBoolean(false);

	protected static class DeviceTwinStatusCallBack implements IotHubEventCallback {

		public void execute(IotHubStatusCode status, Object context) {
			if ((status == IotHubStatusCode.OK) || (status == IotHubStatusCode.OK_EMPTY)) {
				Succeed.set(true);
			} else {
				Succeed.set(false);
			}
			System.out.println("IoT Hub responded to device twin operation with status " + status.name());
		}
	}

	protected static class onProperty implements TwinPropertyCallBack {

		public void TwinPropertyCallBack(Property property, Object context) {
			System.out.println("onProperty callback for " + (property.getIsReported() ? "reported" : "desired")
					+ " property " + property.getKey() + " to " + property.getValue() + ", Properties version:"
					+ property.getVersion());
		}
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		String[] arr = new String[2];

		args = new String[2];

		System.out.println("Starting...");
		System.out.println("Beginning setup.");

		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=2410;SharedAccessKey=F6km/qX9S71fFd2n4USZFhb+ozgR1J85/f8aAji7bIw=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=2347;SharedAccessKey=lfOjSwLc5Mkzvc9Yt9b5AvZF/IgsBfrLwiXpy5mnKQ4=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=7120;SharedAccessKey=XIoxb1hzoI2PoTrxF+W2vNKrmqA1ihmC+5A2Rf1FBos=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=2019;SharedAccessKey=wb6yFfI0RXYOrMdSP+B47TmE0nLe/GBCfrGblGKWbYA=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=7143;SharedAccessKey=KRZWU9H2inuUZAdFZJF4qiYIR5Q6n1pRENFrQkHkYzo=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=583;SharedAccessKey=bh3yTrCp1wiKMA95BNMdCDpJlKkdaRaAUpckDTGoAWU=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=600;SharedAccessKey=22u/++IMiGDu+9dcq2IiBM5PSCG8uS37fT5sZHBHUvQ=";
		
		//***args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=952;SharedAccessKey=02yIQOCERj0nhocBlM3Xjn6dKuSxpVM68OHkvQbKsJw=";
		//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=492;SharedAccessKey=GqaR+vkFRgYUNRmCZ69dx6k1Gi1S3ldS+1v06BssM0o=";
		//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=82652;SharedAccessKey=Ix76pSTMkijJVlSpcqTOxgi1sUeu15Py7SJ8E8Vr1Hc=";
		//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=492;SharedAccessKey=GqaR+vkFRgYUNRmCZ69dx6k1Gi1S3ldS+1v06BssM0o=";
		//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=82800;SharedAccessKey=m5iNGEz9i3IwH7haG7g2nd45bXNb+r3q/0oOHkJLZQw=";
		//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=59661;SharedAccessKey=nSDYfVop1t7oaUvuL/E6gnzCax1JFxnCjpeBXd/wkXM=";
		//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=453;SharedAccessKey=lFrZ4vPdCp4P1buWy2JKuySzc/XwQbwEWdjTIqnRFPc=";
		
		//args[0] ="HostName=SIT-REF-iothub.azure-devices.net;DeviceId=1503;SharedAccessKey=CjTnajqD7EKA0p5MHLORjUSSTAVmJcvaLwL/UQSWG7M=";
		//args[0] ="HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65087;SharedAccessKey=esBA3E7BZlUVWNoo+1CXaRl1k8b4kdYDXAt5FJgbkoo=";
		//args[0] ="HostName=SIT-REF-iothub.azure-devices.net;DeviceId=22236;SharedAccessKey=sV4SA0VHwkD/dwHVSwQ4zpXd2vbvNa3/21/bHR4SeK0=";
		//args[0] ="HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65498;SharedAccessKey=83kKSiimsAQPXlgln22x8UbnsQrWTrxA+RR/WnMr3u4=";
		//args[0] ="HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65814;SharedAccessKey=d+1y4QV+7Dc9AfJvCWjL2OoXIcYsTF/2qF9FEmsLMBg=";
		//args[0] ="HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65858;SharedAccessKey=N84VoYrNm0PQ7Hgp1nH2HTV8Z1p9Zy/wUR2GEs3W0W4=";
		//args[0] ="HostName=SIT-REF-iothub.azure-devices.net;DeviceId=237379;SharedAccessKey=AjUKsxJjeYcH+CQyeacvlcTQzDbRBFdNj4lePzsRmsw=";
		
		//TestDevice
		//args[0] ="HostName=DEV-REF-iothub.azure-devices.net;DeviceId=68790;SharedAccessKey=SqZL/Cor+iHXYCpEKLbC0vyfptA8ZuXlo84XIkQvt3c=";
		args[0] ="HostName=SIT-REF-iothub.azure-devices.net;DeviceId=475655;SharedAccessKey=U6iiUpgZRcCNfmAB4Xh9rB++tprWQIj/Fn6T9yNxtFI=";

		if (args.length < 1) {
			System.out.format("Expected the following argument but received: %d.\n"
					+ "The program should be called with the following args: \n"
					+ "[Device connection string] - String containing Hostname, Device Id & Device Key in the following formats: HostName=<iothub_host_name>;DeviceId=<device_id>;SharedAccessKey=<device_key>\n"
					+ "[Protocol] - (mqtt | amqps | amqps_ws)\n", args.length);
			return;
		}

		String connString = args[0];

		IotHubClientProtocol protocol = IotHubClientProtocol.AMQPS;

		DeviceClient client = new DeviceClient(connString, protocol);
		System.out.println("Successfully created an IoT Hub client.");

		try {
			System.out.println("Open connection to IoT Hub.");
			client.open();

			System.out.println("Start device Twin and get remaining properties...");
			// Properties already set in the Service will shows up in the generic onProperty
			// callback, with value and version.
			Succeed.set(false);
			client.startDeviceTwin(new DeviceTwinStatusCallBack(), null, new onProperty(), null);
			do {
				Thread.sleep(1000);
			} while (!Succeed.get());

			System.out.println("Get device Twin...");
			client.getDeviceTwin(); // For each desired property in the Service, the SDK will call the appropriate
									// callback with the value and version.

			System.out.println("Update reported properties...");

			// String device_public_key = null;

			Set<Property> reportProperties = new HashSet<Property>() {
				{

					JsonObject value1 = new JsonObject();
					
					//value1.addProperty("device_public_key",
						//	"TUlJRnR6Q0NBNStnQXdJQkFnSUNFQUV3RFFZSktvWklodmNOQVFFTEJRQXdNREV1TUN3R0ExVUVBd3dsUVhwMQpjbVVnU1c5VUlFTkJJRlJsYzNSUGJteDVJRWx1ZEdWeWJXVmthV0YwWlNCRFFUQWVGdzB4T1RFd01qUXdPVEEzCk16RmFGdzB4T1RFeE1qTXdPVEEzTXpGYU1CTXhFVEFQQmdOVkJBTU1DRzE1WkdWMmFXTmxNSUlDSWpBTkJna3EKaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUF1NW5MUHF4Uk9QRnUzeXIzN3ZWdDhWL2xUdXdaK1AyawpTejdoZ1pTRDhET2dScm1QWVlxRVhFOVhGZk1kS3R5QWRBdHBnN3h0eVdmbzlvTktKY2traXdKbDhoeFU2VFBkCkdJVzRxUmZTblN6U1NjSUQra0tNamI5RTBEOURwcVJ3ZUdEUDBCV0JoQWxjVzFxa2QzdURka0YwQ1VhZFEwcjcKSXJ4cVFuU2pXOG5ycEx6bVpOZHJTWkFIV3d4TUNSYTZmb0x0cjAzSFRmZGE5V1R2N1BqeGRlOXFJb21xT3NWZQpvZ29HNjdBdlBwZ2x4RDBjZVR0YnltWTZ0bHh4M0dzQUZGUDQ3dWhRdHlhaGE4R2VxWDlZYWZPdFYzbjJUM1RMCnJQaHdRUTN6d0NRVWlnazdhcDBMN0JlR2lWQUVSWWNtaytRdm5ad2d1cjJ0TVVrTWNqakN5aGJBRHREVUlCclMKc3huM09DZEprQitPc3dxa1NYcEpsVWNSK0VQZ2VVeHAyL0ZiQWkwL1BYUWVMeHJuTTNVNUk0U0Rod0UwaVNCZAozZG9PU25qSlcrRmNjTnV0VmpVOVNVSWZNbHpMU0M4d0p2cXc0VUk4ZDk2Y2duLzJuZzQzVjcxZ1BCQUpEckRWCnFmSGpBODhIZDB6aWFyR1d1MGt1ajhYQVVmd3BKYlRUUXdxQ0p5engydndzdis1T3Bmc253QkQ2NDBFNVBrOFQKMzhGQ0RGVngwOHdDTm5Nb2pDcFhzeFpGTnlBcis3azYyL2ppMFMrS2Z1RS9RZXoyT1hBdnZLKzZwNm5kNXNTTwo0SWt6anUzZ0hKYVB2L3dRdXVtd0FmVk0vMndkUmtRd1N4MERlY1ZEVGRlTFFQR0tFUEFKTkk0WmhXOFNQajRoCmUxQitKUGNOdWxVQ0F3RUFBYU9COXpDQjlEQUpCZ05WSFJNRUFqQUFNQkVHQ1dDR1NBR0crRUlCQVFRRUF3SUYKb0RBekJnbGdoa2dCaHZoQ0FRMEVKaFlrVDNCbGJsTlRUQ0JIWlc1bGNtRjBaV1FnUTJ4cFpXNTBJRU5sY25ScApabWxqWVhSbE1CMEdBMVVkRGdRV0JCUm9zRG5JNzZFczRkbWJudHc0UStmT1R0NkcxVEJSQmdOVkhTTUVTakJJCmdCU3IrTjg5Q3hVZ2VoTFZjYkEwR1JQbHI5K05EYUVzcENvd0tERW1NQ1FHQTFVRUF3d2RRWHAxY21VZ1NXOVUKSUVOQklGUmxjM1JQYm14NUlGSnZiM1FnUTBHQ0FoQUFNQTRHQTFVZER3RUIvd1FFQXdJRjREQWRCZ05WSFNVRQpGakFVQmdnckJnRUZCUWNEQWdZSUt3WUJCUVVIQXdRd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dJQkFCYTZYSTA4ClA1R0pCMW56ZVBxZVNOV3dTdjBCL1lsV0FqdEk4MGFTRENIRTdMckF4SEM4M0JRTFhvQWIrWFppOWNsSU9sM1kKZitlSUFGOXhENFZ6SlZud3JoQlNKbERRT3pkN0hjR2pZZmNtSzV1VkU3RnZUTERHUmRaL1ovY1NlWDFyK2lvQwpiaUtMZ2tOZ2NwbktkYWNTaHF4eWJPTHR2TURnSlhWeXNsdTIrS2t0QzRNRTc1YjBoOERDa2VkWms1bjJlQ0pCCmhJVWUyWUpyaWdoT0tTQm43MUVGWEtmL2ZsVDlaKzFrTG1sckQrV2hNdDNKQ2ZnT3NMN0h6bXU1dUhLdEJhZWQKZGNnUnBMQk1ZdFF2d0tXczlkRVpNUFhXOERtNmd1aXZBZWxRdEgzMXh2dFUyd3QxY0gyYXZaM0dMZU5vSitFegpYNXRYbndHb21Gdis2QjBZemw2d09jdzNwRzVKeVpPQnU2cmF6M0dRTm4vYXd3STZFbDloaldCK2NFQkJZN3QvCkIrRVFrWHpZOEkzaSt3WTBBY1pqUUticFg4cXp4cktudmkvVUlYQnBMMzlMUnlkUXdVVzdjUFEranBZWHl5ekwKOXcvRXEvam15WlRUMnBMa1BPU0RtWUFscmxwYjZYeGROaTErcjJFNTlQK1ZmbTlDbkJiT2JWMEw3ejlBQjhQSQp3L09aTFRDdmpUQ1pCTnVyWUtYV1cwSVhVa0phMFh4U0k4ZHRUTU01UWVzVDgxbE9ham1RRmFQOHAvWHVZVWYzCnVFeTJmTUEvWmxBZCtkdW5vSjFSaS9BZzhUU21DY3BMNzQzcW9KbU5LN0laZXN6UEU4dzJ5aXdMMlB1Q2pjK3kKYlQvOEZzNjNGY29tRHZqa1h1ZTVFTjZlNjM1bVN5NVd4azA5");

					//value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBOHVHcWhrbVFnOWR6R0RXTVhZeUsNCmlRUlN1NFYwblNBSi9YVGlHOTNIdzZkQkM5MWlMTHhPc1VSSXpCYWZkb09kMGd4UnhQODUzUkVGOHNCb2h5dTUNCjZqdkNacTV5bGlranNLaC93b3FwREhGK24vWlNjR3JSNzJnSmlZSUNmTTdHWm1ETDJMbDBESytLTUFFcmR0VkMNCkxjZmZ0MG5SWTRMYlpaeHExS2I3aTRiK2RVNUZPMFA2dExacERhTXdLUkFXbjRjZHJsZWdQODRRVWtwM1pmYUcNCnpObHA0T1ZSdDNhNkxRMTdlOU9DUENFeUtHSDQ4VXpTdTE3dnk0RTdIQjRYMDBTSWV1TVpQZFEySnpmL0RiYW0NCnAzL0pMcGZvbFExT2hpRy81aU1FbFV3d0cwb1hrU1pxMUJndFVvZU1nUFZBOUZXOE9od25qbG1lSjkxcUNyTFINCm5RSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					
					//value1.addProperty("device_public_key", "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUEwUDlsQTBBSjY5ZXBFSjBCRmlvdQpQcGR2M0ZaQjN5VWZjdk5NQ1ZnVGMwRGJveXpFcG1veHRXNTFIbnNsRllXeDZ6M1pTaHJ6azk4RHByZXo1MG1WCmdYU1NhckszbXFnbitmL0hiSUNDcHJncnphVUpjK1JVQTBld0lzUkJnQStlYlVKclRNaE1sczZLQzFlOVljNTYKcjluV3drbE56aWUvSStBNXdrZUtoRTVlYlVKblVGQVd2ZWZsVENpTTF1SE56ZFJlb0F6MHl2VHBsYVd2ckplcwpkbDZFVzV4Y2JCc1RxVWlJV1BZZm9TV0hSWG5GcE9URmkvd1kzVFZKWWY4bGZQR1ZMaGR0RjJDbEFxaFFjWmpsCkIyemQwR09yc3RKVFhuYy9nQXFEUEJDcC9ya29mQU91dWp6M3dHeFpnTFFaK2lPRTRzTEs4aTdmT1VuWEFRMXQKOFFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t");
					
					
					//Device 952:-
					//value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBeWExNkhxRms4T3pSR3libStZMzcNCnVMKy9saVZ6TnY3TXd0OWJsZ1hqeTNZcHNsTzIwMlR4YnpsNVFab0pzNU53N09jc0FmTWpNUzlhVlh5YXJUQlgNCkJxeDV3cFN3Z3RmRjlqNTVjSTJLWWhzK2xtMEdrMFF1REg4TTNqTDJNRXBTVDFveThNaTRsRmxaUWpXQlk4NlUNCnVHTjhKUGxJbmI0ZWdLUlZteXo3UjFoQWlEL0dNWHdWcVMySHlUdExmajFWZnFCQ1FSeEZycnZMZW9FYVJkTmUNCjg0TnF3cWhLcmZWV1BWV0lCdmFJbWhvc2dkejZ6M2tHNmNDaWhUVDhMU0dieGpyeE1hcXczQUtoMWNJdFk1dnQNCm5xYnpmcDVoYTZnYTBMOG9XR3RpTjVpcVBSbWlZVFJxcXN6dUpOMER0Z1hYSDBOdHNjU1VscVV4TGVCMHJoaFYNClhRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");

					//Device 1045:invalid RSA-
					//value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBeWExNkhxRms4T3pSR3libStZMzcNCnVMKy9saVZ6TnY3TXd0OWJsZ1hqeTNZcHNsTzIwMlR4YnpsNVFab0pzNU53N09jc0FmTWpNUzlhVlh5YXJUQlgNCkJxeDV3cFN3Z3RmRjlqNTVjSTJLWWhzK2xtMEdrMFF1REg4TTNqTDJNRXBTVDFveThNaTRsRmxaUWpXQlk4NlUNCnVHTjhKUGxJbmI0ZWdLUlZteXo3UjFoQWlEL0dNWHdWcVMySHlUdExmajFWZnFCQ1FSeEZycnZMZW9FYVJkTmUNCjg0TnF3cWhLcmZWV1BWV0lCdmFJbWhvc2dkejZ6M2tHNmNDaWhUVDhMU0dieGpyeE1hcXczQUtoMWNJdFk1dnQNCm5xYnpmcDVoYTZnYTBMOG9XR3RpTjVpcVBSbWlZVFJxcXN6dUpOMER0Z1hYSDBOdHNjU1VscVV4TGVCMHJoaFYNClhRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");

					//Device 492_SIT //Device_82800_SIT
					 // value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBeWExNkhxRms4T3pSR3libStZMzcNCnVMKy9saVZ6TnY3TXd0OWJsZ1hqeTNZcHNsTzIwMlR4YnpsNVFab0pzNU53N09jc0FmTWpNUzlhVlh5YXJUQlgNCkJxeDV3cFN3Z3RmRjlqNTVjSTJLWWhzK2xtMEdrMFF1REg4TTNqTDJNRXBTVDFveThNaTRsRmxaUWpXQlk4NlUNCnVHTjhKUGxJbmI0ZWdLUlZteXo3UjFoQWlEL0dNWHdWcVMySHlUdExmajFWZnFCQ1FSeEZycnZMZW9FYVJkTmUNCjg0TnF3cWhLcmZWV1BWV0lCdmFJbWhvc2dkejZ6M2tHNmNDaWhUVDhMU0dieGpyeE1hcXczQUtoMWNJdFk1dnQNCm5xYnpmcDVoYTZnYTBMOG9XR3RpTjVpcVBSbWlZVFJxcXN6dUpOMER0Z1hYSDBOdHNjU1VscVV4TGVCMHJoaFYNClhRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					  
					//Device 59661_Dev 
					 //   value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBdnYzbXYxT1VyRGphRGk3dU1zMG0NCkd5aklEcnJPNFl3UUthRDkrMGMzdWh3WlU5bUhHWVNaNFk2WkkwSVltMXlnVDA3dkczYzArZUlGMUJKVG1wZ2cNCjh5ckNEY2FjRVZtYUNKL0l3dW1MMU9udHpDeC9TVzMzT1lRcCsxUm10YURjZFBKbFA1U3U2ZGFaRktQdjRkNUENCkUrbk9sWVZ5UU5IZVM1dm9mUk41bUYvUWFYNmZTRitnUG1ERlN5UHFCYVdlNlhVcWVFUlFaYWVZcDlkRGRkWlYNCkVXRTlCRlRKTC92M082SEFUZFdoeFJ0WXR3dHRYQVF1eTUzNFdnVk02bkI4UUhTSzBybE9wUW1lY01iV0hyZHENCk12SitFdEYzRDNzWFNmZmV6dkRKZEw3ekVldmtJaWZpeG1VUW1nSDQyU0dFb0x4OEtHMytUMm5NSWxWZDNwbEUNCmtRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					    
					  //Device 453_Sit
					   // value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBdnYzbXYxT1VyRGphRGk3dU1zMG0NCkd5aklEcnJPNFl3UUthRDkrMGMzdWh3WlU5bUhHWVNaNFk2WkkwSVltMXlnVDA3dkczYzArZUlGMUJKVG1wZ2cNCjh5ckNEY2FjRVZtYUNKL0l3dW1MMU9udHpDeC9TVzMzT1lRcCsxUm10YURjZFBKbFA1U3U2ZGFaRktQdjRkNUENCkUrbk9sWVZ5UU5IZVM1dm9mUk41bUYvUWFYNmZTRitnUG1ERlN5UHFCYVdlNlhVcWVFUlFaYWVZcDlkRGRkWlYNCkVXRTlCRlRKTC92M082SEFUZFdoeFJ0WXR3dHRYQVF1eTUzNFdnVk02bkI4UUhTSzBybE9wUW1lY01iV0hyZHENCk12SitFdEYzRDNzWFNmZmV6dkRKZEw3ekVldmtJaWZpeG1VUW1nSDQyU0dFb0x4OEtHMytUMm5NSWxWZDNwbEUNCmtRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					
					  //Device 453_Sit
					    //value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBdnYzbXYxT1VyRGphRGk3dU1zMG0NCkd5aklEcnJPNFl3UUthRDkrMGMzdWh3WlU5bUhHWVNaNFk2WkkwSVltMXlnVDA3dkczYzArZUlGMUJKVG1wZ2cNCjh5ckNEY2FjRVZtYUNKL0l3dW1MMU9udHpDeC9TVzMzT1lRcCsxUm10YURjZFBKbFA1U3U2ZGFaRktQdjRkNUENCkUrbk9sWVZ5UU5IZVM1dm9mUk41bUYvUWFYNmZTRitnUG1ERlN5UHFCYVdlNlhVcWVFUlFaYWVZcDlkRGRkWlYNCkVXRTlCRlRKTC92M082SEFUZFdoeFJ0WXR3dHRYQVF1eTUzNFdnVk02bkI4UUhTSzBybE9wUW1lY01iV0hyZHENCk12SitFdEYzRDNzWFNmZmV6dkRKZEw3ekVldmtJaWZpeG1VUW1nSDQyU0dFb0x4OEtHMytUMm5NSWxWZDNwbEUNCmtRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					
					 // add(new Property("security", value1));

					//Device 22236
					    //value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBbXg3Nm10eU51YUZna0c4dXdNU1ANCmY5dXpJQjNSUUNBZkVIbWRVM3ByM2xMWkxKdEhqNkNGd2lIVWl5Rkg1b0FnbmcxT1EwdlBGajE0RXNkOHBjdWgNClNSKzZqcGNFSzVtR1M1c0M1dVJxTVR6RVhxaENQdnlGOVY0ejJ5dm1TTDZpMnBQNWFTMGNZL3dLMHV0Vzc4bXgNCmlrZE5BbDBjQ1FtNnpkMWkxVDF2cHJlV3FWL3FVbWRuY1BTdElOUXJNM0xoTU45cExleHRuSlZqT0JxS2lsKy8NCkxPTnhXeHRKTzJ5R0tObUVNeW5nRWE1eERERllGSWlzSFlJdVA2VlhlZTJPd3kvMkp4SmpSQlFVZXgxZDYwdHINClFHazkyWVlaL0ZFZUp3cDZmZjFhb280MUFJZWwxalVURnQ4NmtPS3EwWHMvdmFQUDdWcnIrQ2ZtcGlya1FCUWYNClNRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					   
					//Invalid RSA
					//value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0");
					
					//value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBdnYzbXYxT1VyRGphRGk3dU1zMG0NCkd5aklEcnJPNFl3UUthRDkrMGMzdWh3WlU5bUhHWVNaNFk2WkkwSVltMXlnVDA3dkczYzArZUlGMUJKVG1wZ2cNCjh5ckNEY2FjRVZtYUNKL0l3dW1MMU9udHpDeC9TVzMzT1lRcCsxUm10YURjZFBKbFA1U3U2ZGFaRktQdjRkNUENCkUrbk9sWVZ5UU5IZVM1dm9mUk41bUYvUWFYNmZTRitnUG1ERlN5UHFCYVdlNlhVcWVFUlFaYWVZcDlkRGRkWlYNCkVXRTlCRlRKTC92M082SEFUZFdoeFJ0WXR3dHRYQVF1eTUzNFdnVk02bkI4UUhTSzBybE9wUW1lY01iV0hyZHENCk12SitFdEYzRDNzWFNmZmV6dkRKZEw3ekVldmtJaWZpeG1VUW1nSDQyU0dFb0x4OEtHMytUMm5NSWxWZDNwbEUNCmtRSURBUUFCDQotLS0tLUVORCBQVUJMSUMgS0VZLS0tLS0NCg==");
					value1.addProperty("device_public_key","LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF3Y1h0bnE3Qzd3dmhnbEx6NkZYWgpTS0FuZ3FKM2RwY3BiYlUyR3ZaZzREbUpqQ2dzWklpY3EwcmZyNTNJN3VaWUlZQW9QU1pjbGxBRjlDT25Gemp0Ci9HK0ZjQVA3OHd6b3FWMmFGMnlpM1owOEVzdDRjeDdNbUZMSlRsREVhYU1DVEFBQXBTYko5cXdBcTl2c2pyL0oKN3FaSGhjbTBsMDRGY0VZa1NncnNrc1BlSjlPUUlpVzdFT2x6ZXl5Snh1UnFNMHgwdWlvWXJkaUNYUzJuQitQUgpRL3lVSWQwNUlLdUh5MzEyalJ3ajlMR01UcHc0YXVFeFlWSTB4b1FqTEk1WUoyNEdPUDhOSGZMZ2Yra1RRZ3J2Ck9aaVlFTTh6cjV3RGhaT1ZJenV3dUtxcGJhNzNYb2V2dU84SW5BVllOSHUwcnpHOStxMHF2RWpNaDl6ZDd1TCsKMndJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==");
					add(new Property("security", value1));
				}
			};
			client.sendReportedProperties(reportProperties);

			System.out.println("Waiting for Desired properties");
			
		} catch (Exception e) {
			System.out.println("On exception, shutting down \n" + " Cause: " + e.getCause() + " \n" + e.getMessage());
			client.closeNow();
			System.out.println("Shutting down...");
		}

		System.out.println("Press any key to exit...");

		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();

		client.closeNow();

		System.out.println("Shutting down...");

	}

}
