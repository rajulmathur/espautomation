package FileEncryptionTestCode;


import com.microsoft.azure.sdk.iot.device.DeviceClient;
import com.microsoft.azure.sdk.iot.device.IotHubClientProtocol;
import com.microsoft.azure.sdk.iot.device.IotHubEventCallback;
import com.microsoft.azure.sdk.iot.device.IotHubStatusCode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

/**
 * File Upload Simple Sample for an IoT Hub. This is a sample
 * that upload a single file to blob using the IoT Hub.
 */

public class FileUpload {
	
	/**
     * Upload a single file to blobs using IoT Hub.
     *
     * @param args
     * args[0] = IoT Hub connection string
     * args[1] = File to upload
     */
	static long start = System.currentTimeMillis();
	//static String fileName = "CASE-607\\robot-videos-main\\207e927f-8261-49f6-8fe6-07b1676ba318abcd.zip";
    public static void main(String[] args)
            throws IOException, URISyntaxException
    {
    	String[] arr = new String[2];
    	
    	args = new String[2];
    	
    	//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=581;SharedAccessKey=+0586fLrHgTJW1SqtzZOBnyloprWht6eAQ8tbTeoTgI=";
    	//demo env device
    	//args[0] = "HostName=DEMO-REF-iothub.azure-devices.net;DeviceId=602;SharedAccessKey=NXEpOXm3CYs/FJwVtaSxc8Ur0ijRDvZO5zCg8KSOZmA=";
    //	args[1] = "C:\\WORK\\JnJ\\UserStories\\PI2\\FileEncryption\\SampleFiles\\CASEID-f9010bab-f08a-4789-b1c0-cf5e4ea54123.encrypted";
    	
    	//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=952;SharedAccessKey=02yIQOCERj0nhocBlM3Xjn6dKuSxpVM68OHkvQbKsJw=";
    	//args[1] = "C:\\FileEncryptionTests\\Dev952\\File\\sample1\\video\\enc\\f9010bab-f08a-4789-b1c0-cf5e4ea54101.zip";
    	
    	//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=453;SharedAccessKey=lFrZ4vPdCp4P1buWy2JKuySzc/XwQbwEWdjTIqnRFPc=";
    	//args[1] = "C:\\FileEncryptionTests\\Sit_39\\file\\Output\\f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
    
    	//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=1503;SharedAccessKey=CjTnajqD7EKA0p5MHLORjUSSTAVmJcvaLwL/UQSWG7M=";
    	//args[1] = "C:\\FileEncryptionTests\\Sit_1503\\file\\Output\\f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
    	
    	
    	
    	//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=22236;SharedAccessKey=sV4SA0VHwkD/dwHVSwQ4zpXd2vbvNa3/21/bHR4SeK0=";
    	//args[1] = "C:\\FileEncryptionTests\\Sit_22236\\file\\Output\\f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
    	
    	//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=5652;SharedAccessKey=2lEb20U6M6Ot16jTuo06USM3xxYs7cXJRYP7syMGBm4=";
    	//args[1] = "C:\\FileEncryptionTests\\sample\\00c648ae-646a-11ea-8daa-acde48001122.zip";
    	
    	//args[0] = "HostName=SIT-REF-iothub.azure-devices.net;DeviceId=237379;SharedAccessKey=AjUKsxJjeYcH+CQyeacvlcTQzDbRBFdNj4lePzsRmsw=";
    	//args[1] = "C:\\FileEncryptionTests\\SIT_237379\\file\\Output\\f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
    	
    	//args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65087;SharedAccessKey=esBA3E7BZlUVWNoo+1CXaRl1k8b4kdYDXAt5FJgbkoo=";
    	//args[1] = "C:\\FileEncryptionTests\\Dev\\ForNagashree\\Video_1280x720_1mb.mp4";
    	
    	args[0] = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=65087;SharedAccessKey=esBA3E7BZlUVWNoo+1CXaRl1k8b4kdYDXAt5FJgbkoo=";
    	args[1] = "C:\\FileEncryptionTests\\Dev\\file2\\output\\f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
    	
    	
        String connString = null;
        String fullFileName = null;

        System.out.println("Starting...");
        System.out.println("Beginning setup.");


        if (args.length == 2)
        {
        	
           connString = args[0];
          fullFileName = args[1];
           //fullFileName = "C:\\Users\\shuchi.puneeth\\Documents\\PI2DeviceScrum\\File Encryption\\" + fileName;
           
           
           // connString = "HostName=DEV-REF-iothub.azure-devices.net;DeviceId=975;SharedAccessKey=eM2Ppoq+svYqMya/RyQQxr1TrEBGiF3bhyZoBIS5idg=";
           // fullFileName = "C:\\Users\\shuchi.puneeth\\Downloads\\response_1570456973443.json";
            
        }
        else
        {
            System.out.format(
                    "Expected the following argument but received: %d.\n"
                            + "The program should be called with the following args: \n"
                            + "[Device connection string] - String containing Hostname, Device Id & Device Key in the following formats: HostName=<iothub_host_name>;DeviceId=<device_id>;SharedAccessKey=<device_key>\n"
                            + "[File to upload] - String containing the full path for the file to upload.\n",
                    args.length);
            return;
        }

        // File upload will always use HTTPS, DeviceClient will use this protocol only
        //   for the other services like Telemetry, Device Method and Device Twin.
        IotHubClientProtocol protocol = IotHubClientProtocol.AMQPS;

        System.out.println("Successfully read input parameters.");
        System.out.format("Using communication protocol %s.\n",
                protocol.name());

        DeviceClient client = new DeviceClient(connString, protocol);

        System.out.println("Successfully created an IoT Hub client.");
       // System.out.println(client.uploadToBlobAsync);
        
        try
        {

            File file = new File(fullFileName);
            //String destination = "CASE-1011/robot-videos-main/CASEID-f9010bab-f08a-4789-b1c0-cf5e4ea54123.encrypted";
            //String destination = "CASE-1011/robot-videos-main/f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip.enc";
            //String destination = "CASE-65089/robot-videos-main/f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
            //String destination = "22307/robot-videos-main/f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
           // String destination = "00c648ae-646a-11ea-8daa-acde48001122.zip";
             //String destination = "237381/robot-videos-main/f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
            //String destination = "N/Video_1280x720_1mb.mp4";
            String destination = "C/65089/robot-videos-main/f9010bab-f08a-4789-b1c0-cf5e4ea54100.zip";
            
            if(file.isDirectory())
            {
                throw new IllegalArgumentException(fullFileName + " is a directory, please provide a single file name, or use the FileUploadSample to upload directories.");
            }
            else
            {
            	//static String fileName = "CASE-607\\robot-videos-main\\207e927f-8261-49f6-8fe6-07b1676ba318abcd.zip";
            	client.uploadToBlobAsync(destination, new FileInputStream(file), file.length(), new FileUploadStatusCallBack(), null);

          //client.uploadToBlobAsync(file.getName(), new FileInputStream(file), file.length(), new FileUploadStatusCallBack(), null);
            }

            System.out.println("Input File size: " + file.length() );
            System.out.println("File upload started with success");

            System.out.println("Waiting for file upload callback with the status...");
            System.out.println("Uploaded file: " + file.getName());
        }
        catch (Exception e)
        {
            System.out.println("On exception, shutting down \n" + " Cause: " + e.getCause() + " \nERROR: " +  e.getMessage());
            System.out.println("Shutting down...");
            client.closeNow();
        }

        System.out.println("Press any key to exit...");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        System.out.println("Shutting down...");
        client.closeNow();
    }

    protected static class FileUploadStatusCallBack implements IotHubEventCallback
    {
        public void execute(IotHubStatusCode status, Object context)
        {
            System.out.println("IoT Hub responded to file upload operation with status " + status.name());
            
            long end = System.currentTimeMillis();
            System.out.println("Time Taken to Upload File in milliseconds: " + 
                    (end - start) + "ms"); 
        }
        
    }
    
}
