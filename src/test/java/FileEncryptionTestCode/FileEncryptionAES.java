package FileEncryptionTestCode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class FileEncryptionAES {
	
	static void fileProcessor(int cipherMode,String key,File inputFile,File outputFile) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		 try {
		       Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
		       Cipher cipher = Cipher.getInstance("AES");
		       cipher.init(cipherMode, secretKey);

		       FileInputStream inputStream = new FileInputStream(inputFile);
		       byte[] inputBytes = new byte[(int) inputFile.length()];
		       inputStream.read(inputBytes);

		       byte[] outputBytes = cipher.doFinal(inputBytes);

		       FileOutputStream outputStream = new FileOutputStream(outputFile);
		       outputStream.write(outputBytes);

		       inputStream.close();
		       outputStream.close();

		    } 
		 catch (IOException e) {
			e.printStackTrace();
	            }
	     }
		
	public static void main(String[] args) {
		
		/*
		 * # ---- Use a 256-bit KEK to wrap a 256-bit AES key. # kek =
		 * 000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F # keyData =
		 * 00112233445566778899AABBCCDDEEFF000102030405060708090A0B0C0D0E0F # expected =
		 * 28C9F404C4B810F4CBCCB35CFB87F8263F5786E2D80ED326CBC7F0E71A99F43BFB988B9B7A02DD21
		 * # computed =
		 * 28C9F404C4B810F4CBCCB35CFB87F8263F5786E2D80ED326CBC7F0E71A99F43BFB988B9B7A02DD21
		 * # unwrapped =
		 * 00112233445566778899AABBCCDDEEFF000102030405060708090A0B0C0D0E0F
		 */
		
		
		//String key = "AES Key for Device 952";
		String AESkey = "4c1c1b1f53e89f25d3ceabfb0734cdd2e1cfc5bbb642107c44dafb45afb9c59c";
		File inputFile = new File("C:\\WORK\\JnJ\\UserStories\\PI2\\FileEncryption\\SampleFiles\\f9010bab-f08a-4789-b1c0-cf5e4ea54123.zip");
		File encryptedFile = new File("C:\\WORK\\JnJ\\UserStories\\PI2\\FileEncryption\\SampleFiles\\CASEID-f9010bab-f08a-4789-b1c0-cf5e4ea54123.encrypted");
		File decryptedFile = new File("C:\\WORK\\JnJ\\UserStories\\PI2\\FileEncryption\\SampleFiles\\decryptedfile.zip");
			
		try {
		     FileEncryptionAES.fileProcessor(Cipher.ENCRYPT_MODE,AESkey,inputFile,encryptedFile);
		     FileEncryptionAES.fileProcessor(Cipher.DECRYPT_MODE,AESkey,encryptedFile,decryptedFile);
		     System.out.println("Wrapped AES key used for Encryption: " + AESkey);
		     System.out.println("File Encryption Success");
		     System.out.println("Encrypted file name: " + encryptedFile.getName());
		 } catch (Exception ex) {
		     System.out.println(ex.getMessage());
	             ex.printStackTrace();
		 }
	     }

}
